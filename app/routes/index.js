const express = require('express');
const fs = require('fs');
const path = require('path');
const router = express.Router();

// Load all controller in app
console.log('# Init Controllers!');
const files = fs.readdirSync("routes");
files.forEach((file) => {
  const nameController = path.parse(file).name;
  if (nameController != "index" && nameController != "root") {
    router.use(require('./' + nameController));
  }
});

module.exports = router;

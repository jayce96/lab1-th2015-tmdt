const express =  require('express');
const router = express.Router();

module.exports = class RootRouter { 
    constructor() {
        this.router = router;
    }
}


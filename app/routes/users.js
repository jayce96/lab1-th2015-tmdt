const RootRouter = require('./root');

class UserRouter extends RootRouter {

  constructor() {
    super();
    return this.router;
  }

  static getUserRouter() {
    return new UserRouter();
  }
}

module.exports = UserRouter.getUserRouter();


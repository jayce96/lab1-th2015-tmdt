const RootRouter = require('./root');

class Lab1Router extends RootRouter {

    constructor() {
        super();
        this.router.get('/GetValue', this.handleGetValue.bind(this));
        this.router.post('/Cong/a/:value_a/b/:value_b', this.handleAddTwoNumber.bind(this));
        this.router.put('/Tru/a/:value_a/b/:value_b', this.handleSubTwoNumber.bind(this));
        this.router.delete('/Nhan/a/:value_a/b/:value_b', this.handleMulTwoNumber.bind(this));
        this.router.delete('/Chia/a/:value_a/b/:value_b', this.handleDevTwoNumber.bind(this));
        this.router.get('/getThongTin', this.hanldeGetInformation.bind(this));
        return this.router;
    }

    handleGetValue(req, res, next) {
        return res.json({
            payload: {},
            msg: "Hello World !!! – " + new Date().toString(),
            error: false
        })
    }


    handleAddTwoNumber(req, res, next) {

        const num_a = parseFloat(req.params.value_a);
        const num_b = parseFloat(req.params.value_b);

        const result = parseFloat(num_a) + parseFloat(num_b);

        return res.json({
            payload: result,
            msg: '',
            error: false
        })

    }

    handleSubTwoNumber(req, res, next){

        const num_a = parseFloat(req.params.value_a);
        const num_b = parseFloat(req.params.value_b);

        const result = parseFloat(num_a) - parseFloat(num_b);

        return res.json({
            payload: result,
            msg: '',
            error: false
        })
    }

    handleMulTwoNumber(req, res, next) {

        const num_a = parseFloat(req.params.value_a);
        const num_b = parseFloat(req.params.value_b);

        const result = parseFloat(num_a) * parseFloat(num_b);

        return res.json({
            payload: result,
            msg: '',
            error: false
        })
    }

    handleDevTwoNumber(req, res, next){

        const num_a = parseFloat(req.params.value_a);
        const num_b = parseFloat(req.params.value_b);
        

        if(num_b == 0) {
            return res.json({
                payload: null,
                msg: 'Không được thực hiện phép chia 0',
                error: false
            })
        }
        else {
            const result = num_a / num_b;
            return res.json({
                payload: result,
                msg: '',
                error: false
            })
        }
    }

    hanldeGetInformation(req, res, next){
        const result = {
            MSSV: '1412390',
            HoTen: 'Thai Nguyen Hoang Phat',
            Email: 'thainguyenhoangphatit@gmail.com'
        }
        return res.json({
            payload: result,
            msg: '',
            error: false
        })
    }

    static getLab1Router() {
        return new Lab1Router();
    }
}

module.exports = Lab1Router.getLab1Router();
